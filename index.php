<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Third day PHP</title>
</head>
<body>

<h3>DivisibleBy5</h3>
    <?php divisibleBy5(); ?>

    <h3>Add to Array</h3>
    <?php array_push($students, "John Smith") ?>
    <pre> <?php var_dump($students); ?></pre>
    <p><?= count($students);?></p>

    <?php array_push($students, "Jane Smith") ?>
    <pre> <?php var_dump($students); ?></pre>
    <p><?= count($students);?></p>

    <?php array_shift($students) ?>
    <pre> <?php var_dump($students); ?></pre>
    <p><?= count($students);?></p>
</body>
</html>